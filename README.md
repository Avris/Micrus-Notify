## Micrus Notify Module ##

This is a module for [Micrus framework](https://micrus.avris.it)
that automatically sends you emails whenever an exception occurs.

To install this module, open the file `app/Config/modules.yml` and add:

     - Avris\Micrus\Notify\NotifyModule

Then run:

    composer require avris/micrus-notify

### Config ###

Most likely you'd only like to have this feature on the `prod` environment.
In this case modify the `config_prod.yml` file to include an entry like that:

    exceptionNotify:
        enabled: true
        appName: MyApp
        admins:
            Admin: email@example.com

You can also include additional config:

**aggregate** (default: `3600` seconds = 1 hour) -- to avoid spamming your inbox,
if the same exception happens multiple times during the given amount of time,
you'll only be notified once.

**spool** (default: `true`) -- see: [Micrus-Mailer#spooling](https://gitlab.com/Avris/Micrus-Mailer#spooling)

**excludeCodes**: (default: `[403, 404]`) -- ignore the listed HTTP response codes

**excludeClasses**: (default: `[]`) -- list of strings with classnames of exceptions that should be ignored

**excludeUrls**: (default: `[]`) -- list of strings with regex patterns for the URLs that should be ignored

**excludeAgents**: (default: `[]`) -- list of strings with regex patterns for the user agents (bots etc.) that should be ignored

**excludeIps**: (default: `[]`) -- list of strings with IPs that should be ignored

### Example email notification ###

![Example email](src/Asset/example-mail-sm.png)

### Copyright ###

* **Author:** Andrzej Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
