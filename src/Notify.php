<?php
namespace Avris\Micrus\Notify;

use Avris\Micrus\Controller\Http\RequestInterface;
use Avris\Micrus\Exception\Handler\ErrorEvent;
use Avris\Micrus\Exception\Handler\ErrorHandler;
use Avris\Micrus\Exception\Handler\ErrorRenderer;
use Avris\Micrus\Mailer\Mail\Address;
use Avris\Micrus\Mailer\MailBuilder;
use Avris\Micrus\Mailer\Mailer;
use Avris\Bag\Bag;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class Notify
{
    /** @var Bag */
    protected $config;

    /** @var Mailer */
    protected $mailer;

    /** @var MailBuilder */
    protected $mailBuilder;

    /** @var RequestInterface */
    protected $request;

    /** @var Aggregator */
    protected $aggregator;

    /** @var VarDumper */
    protected $dumper;

    /** @var LoggerInterface */
    protected $logger;

    public function __construct(
        Bag $config,
        Mailer $mailer,
        MailBuilder $mailBuilder,
        RequestInterface $request,
        Aggregator $aggregator,
        VarDumper $dumper,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->mailer = $mailer;
        $this->mailBuilder = $mailBuilder;
        $this->request = $request;
        $this->aggregator = $aggregator;
        $this->dumper = $dumper;
        $this->logger = $logger;
    }

    public function onError(ErrorEvent $event)
    {
        $this->handle($event->getException());
    }

    public function handle(\Exception $e)
    {
        $this->logger->log(LogLevel::ERROR, $e, ['request' => $this->request]);

        if ($this->config->get('rethrow')) {
            throw $e;
        }

        if (!$this->shouldHandle($e)) {
            return;
        }

        $mail = $this->mailBuilder->build(
            'exceptionNotify',
            [
                'appName' => $this->config->get('appName'),
                'exception' => $e,
                'exceptionClass' => (new \ReflectionClass($e))->getShortName(),
                'exceptionRendered' => ErrorRenderer::renderDevError($e),
                '%exceptionRendered%' => ErrorRenderer::renderDevError($e, 'raw'),
                'generatedAt' => new \DateTime(),
                'url' => $this->request->getAbsoluteBase() . $this->request->getUrl(),
                'request' => $this->request,
                '%request%' => $this->dumper->dump($this->request, false),
            ],
            file_get_contents(__DIR__ . '/Asset/style.css')
        );

        foreach ($this->config->get('admins', []) as $name => $email) {
            $mail->addTo(new Address($email, $name));
        }

        $mail->embedImage('logo', __DIR__ . '/Asset/logo.png');

        $this->mailer->send($mail, $this->config->get('spool'));
    }

    protected function shouldHandle(\Exception $e)
    {
        if (!$this->config->get('enabled')) {
            return false;
        }

        if (empty($this->config->get('admins'))) {
            return false;
        }

        if (in_array(ErrorHandler::clearStatusCode($e->getCode()), $this->config->get('excludeCodes'))) {
            return false;
        }

        if (in_array(get_class($e), $this->config->get('excludeClasses'))) {
            return false;
        }

        $url = $this->request->getCleanUrl();
        foreach ($this->config->get('excludeUrls') as $pattern) {
            if (preg_match('#'.$pattern.'#', $url)) {
                return false;
            }
        }

        $agent = $this->request->getHeaders('user-agent');
        foreach ($this->config->get('excludeAgents') as $pattern) {
            if (preg_match('#'.$pattern.'#', $agent)) {
                return false;
            }
        }

        if (in_array($this->request->getIp(), $this->config->get('excludeIps'))) {
            return false;
        }

        if (!$this->aggregator->aggregate($e)) {
            return false;
        }

        return true;
    }
}
