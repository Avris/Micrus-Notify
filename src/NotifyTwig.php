<?php
namespace Avris\Micrus\Notify;

use Avris\Micrus\View\TemplateDirsProvider;

class NotifyTwig implements TemplateDirsProvider
{
    public function getTemplateDirs()
    {
        return __DIR__ . '/View';
    }
}
