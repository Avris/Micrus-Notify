<?php
namespace Avris\Micrus\Notify;

use Avris\Micrus\Exception\ConfigException;

class Aggregator
{
    /** @var int */
    protected $forSeconds;

    /** @var string */
    protected $dir;

    /**
     * @param int $forSeconds
     * @param string $dir
     * @throws ConfigException
     */
    public function __construct($forSeconds, $dir)
    {
        if (!$forSeconds) {
            return;
        }

        $this->forSeconds = $forSeconds;
        $this->dir = $dir;

        if (!is_dir($this->dir)) {
            if (!@mkdir($this->dir, 0777, true)) {
                throw new ConfigException('Cannot write to notify aggregator dir in ' . $dir);
            }
        }
    }

    public function aggregate(\Exception $e)
    {
        if (!$this->forSeconds) {
            return true;
        }

        $hash = sha1(get_class($e) . '|' . $e->getFile());

        $path = $this->dir . '/' . $hash;

        if (!file_exists($path)) {
            file_put_contents($path, time());
            return true;
        }

        $createdAt = (int) file_get_contents($path);

        if (time() - $createdAt > $this->forSeconds) {
            file_put_contents($path, time());
            return true;
        }

        return false;
    }

}
