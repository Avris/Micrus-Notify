<?php
namespace Avris\Micrus\Notify;

use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;

class VarDumper
{
    /** @var VarCloner */
    protected $cloner;

    /** @var HtmlDumper */
    protected $htmlDumper;

    /** @var CliDumper */
    protected $cliDumper;

    public function __construct()
    {
        $this->cloner = new VarCloner();
        $this->htmlDumper = new HtmlDumper();
        $this->cliDumper = new CliDumper();
    }

    public function dump($var, $html = true)
    {
        $dumper = $html ? $this->htmlDumper : $this->cliDumper;

        return $dumper->dump($this->cloner->cloneVar($var), true);
    }
}
