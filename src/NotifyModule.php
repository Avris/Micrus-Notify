<?php
namespace Avris\Micrus\Notify;

use Avris\Micrus\Bootstrap\Module;
use Avris\Micrus\Localizator\Locale\YamlLocaleSet;

class NotifyModule implements Module
{
    public function extendConfig($env, $rootDir)
    {
        return [
            'services' => [
                'exceptionNotify' => [
                    'class' => Notify::class,
                    'params' => [
                        '@config.exceptionNotify',
                        '@mailer',
                        '@mailBuilder',
                        '@request',
                        '@exceptionNotifyAggregator',
                        '@varDumper',
                        '@logger',
                    ],
                    'events' => ['error'],
                ],
                'exceptionNotifyTwig' => [
                    'class' => NotifyTwig::class,
                    'tags' => ['templateDirs'],
                ],
                'exceptionNotifyLocaleSet' => [
                    'class' => YamlLocaleSet::class,
                    'params' => ['exceptionNotify', __DIR__ . '/Locale', 'en'],
                    'tags' => ['localeSet'],
                ],
                'exceptionNotifyAggregator' => [
                    'class' => Aggregator::class,
                    'params' => ['@config.exceptionNotify.aggregate', '{@rootDir}/run/notify/{@env}'],
                ],
                'varDumper' => [
                    'class' => VarDumper::class,
                ],
            ],
            'exceptionNotify' => [
                'enabled' => false,
                'rethrow' => false,
                'appName' => 'Micrus App',
                'admins' => [],
                'aggregate' => 3600,
                'spool' => true,
                'excludeCodes' => [403, 404],
                'excludeClasses' => [],
                'excludeUrls' => [],
                'excludeAgents' => [],
                'excludeIps' => [],
            ],
        ];
    }
}
